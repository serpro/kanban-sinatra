# Em MongoDB 2.4


## Habilitando a busca no MongoDB
Adicionar argumento --setParameter textSearchEnabled=true na inicialização do daemon 'mongod'

## Criação de índices
### Indíce de texto para todas as colunas

db.tasks.ensureIndex( { "$**": "text" } )


### Índice de texto para a coluna text
db.tasks.ensureIndex( { "text": "text" } )

## Realizando a busca
db.tasks.runCommand("text", {search: 'atividade'} )




