myApp.filter('newlines', function() {
  return function(text) {
    return (text === undefined ? undefined :  text.replace(/\n/g, '<br/>'));
  }
})

myApp.filter('placeholder', function() {
  return function(text, placeholder) {
    console.log('here text -- ' + text.length);
    console.log('here placeholder -- ' + placeholder);
    if(text === undefined || text == null || text == '' || text == ' '){
      return placeholder;
    }
    return text;
  }
})

myApp.filter('noHTML', function() {
  return function(text) {
    return text.replace(/&/g, '&amp;')
      .replace(/>/g, '&gt;')
      .replace(/</g, '&lt;');
  }
});

myApp.filter('unsafe', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}]);


myApp.filter('getBy', function() {
  return function(field_name, input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (input[i][field_name] === id) {
        return input[i];
      }
    }
    return null;
  }
});