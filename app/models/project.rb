class Project
  include Mongoid::Document
  field :name, type: String
  field :type, type: String, :default => 'project'
  field :status_set, type: String
  embeds_many :kanbans

  validates_uniqueness_of :name
  validates_presence_of :name

  default_scope excludes(type: "template")

  def self.default_template
    self.find_by(name: 'default_template', type: "template")
  end

  def self.templates
    self.where(type: 'template')
  end

  def create_kanban(kanban)
    novo_kanban = self.kanbans.new(name: kanban["name"], tasks_fields: kanban["tasks_fields"])
    unless (kanban["status"])
      if(status_set.blank?)
        Status.default_status_set.each do |status_name|
          novo_kanban.status.new(name: status_name)
        end
      else
        statuses = status_set.split(',').map {|x| x.strip }
        statuses.each do |status_name|
          novo_kanban.status.new(name: status_name)
        end
      end
    end
    self.save!
    novo_kanban
  end

  def self.transform_status_set(status_set)
    if status_set.is_a? Array
      status_set.join ','
    else
      status_set.to_s
    end
  end
end