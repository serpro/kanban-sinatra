class Kanban
  include Mongoid::Document
  field :name, type: String
  embedded_in :project
  embeds_many :status
  has_many :tasks

  validates_uniqueness_of :name
  validates_presence_of :name

  validate :check_first_task_field_name

  def to_json(options={})
    super(options.merge({:include => :tasks}))
  end

  def change_status_name(status)
    status_to_change = self.status.find status['_id']
    status_to_change.name = status['name']
    status_to_change.save! 
  end

  def add_status(params_hash)
    new_status = status.new(name: params_hash['new_status'])
    save!
    { :_id => self._id, :new_status => new_status }
  end
  
  def move_task(task, status_to, position_to)
    return if task.kanban_id != self.id   #only move tasks from this kanban
    return if status_to.kanban.id != self.id #only if the status_to is within this kanban
    #remove task from original status group
    status_from = status.find task.status_id
    status_from.tasks.delete task
    #assign new task sequential
    task.seq = position_to
    #push task to target status group
    status_to.tasks.push task
  end

  # validation to keep first kanban field name unchanged as 'text'
  def check_first_task_field_name
    return true unless attributes['tasks_fields'].present?
    if attributes['tasks_fields'].length > 0 && ! "text".eql?(attributes['tasks_fields'][0]['name'])
      errors.add('tasks_fields', 'Field text can not be changed')
    end
  end

  def delete_status params_hash
    status_id = params_hash['status_deleted']
    if attributes['status_deleted']
      add_to_set(:status_deleted, status_id) unless  attributes['status_deleted'].include? status_id
    else
      update_attributes!(:status_deleted => [status_id])
    end
    reload
    { :_id => self._id, :status_deleted => attributes['status_deleted'] }
  end

  def restore_status params_hash
    status_id = params_hash["status_deleted"]
    pull(:status_deleted, status_id)
    { :_id => self._id, :status_deleted => attributes['status_deleted'] }
  end


end